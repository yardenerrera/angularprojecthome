import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Users } from './interfaces/users';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

apiUrl='https://jsonplaceholder.typicode.com/users/';

  constructor(private http: HttpClient, private db: AngularFirestore) { }

  getUsers(){
  return this.http.get<Users[]>(this.apiUrl)
  }

  addUser(name:string){
    const user = {name:name}
    this.db.collection('posts').add(user)  
  }  
}
