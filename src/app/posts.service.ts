import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Posts } from './interfaces/posts';
import { AngularFirestore } from '@angular/fire/firestore';
import { Users } from './interfaces/users';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiUrl = "https://jsonplaceholder.typicode.com/posts/"

  
  constructor(private http: HttpClient, private db: AngularFirestore) { }

  getPosts(){
    return this.http.get<Posts[]>(this.apiUrl);
  }
  addPosts(title:string,  author:string, body:string){
    const post = {title:title, author:author, body:body}
    this.db.collection('posts').add(post)  
  }  
}
